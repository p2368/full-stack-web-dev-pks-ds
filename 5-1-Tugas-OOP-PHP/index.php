<?php

abstract class Hewan
{
    protected $jenisHewan, $darah = 50, $jumlahKaki, $keahlian;

    protected function atraksi()
    {
        return $this->jenisHewan . " sedang " . $this->keahlian;
    }
}

abstract class Fight extends Hewan
{
    protected $attackPower, $defencePower;

    public function serang($musuh)
    {
        echo $this->jenisHewan . " sedang menyerang " . $musuh->jenisHewan . "<br>";
        $this->diserang($musuh);
    }
    private function diserang($musuh)
    {
        echo $musuh->jenisHewan . " sedang diserang<br>";
        $musuh->darah -= ($this->attackPower / $musuh->defencePower);
    }
}


class Elang extends Fight
{

    public function __construct()
    {
        $this->jenisHewan = "Elang";
        $this->darah;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan = " . $this->jenisHewan . "<br>";
        echo "Jumlah Kaki = " . $this->jumlahKaki . "<br>";
        echo "Keahlian = " . $this->atraksi() . "<br>";
        echo "Darah = " . $this->darah . "<br>";
        echo "attackPower = " . $this->attackPower . "<br>";
        echo "defencePower = " . $this->defencePower . "<br>";
    }
}

class Harimau extends Fight
{

    public function __construct()
    {
        $this->jenisHewan = "Harimau";
        $this->darah;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan = " . $this->jenisHewan . "<br>";
        echo "Jumlah Kaki = " . $this->jumlahKaki . "<br>";
        echo "Keahlian = " . $this->keahlian . "<br>";
        echo "Darah = " . $this->darah . "<br>";
        echo "attackPower = " . $this->attackPower . "<br>";
        echo "defencePower = " . $this->defencePower . "<br>";
    }
}

$elang = new Elang;
$harimau = new Harimau;

$harimau->getInfoHewan();
echo "<br>";

for ($i = 0; $i < 5; ++$i) {
    $elang->serang($harimau);
    echo "<br>";
    $harimau->getInfoHewan();
    echo "<br>";
}

echo "<hr>";

$elang->getInfoHewan();
echo "<br>";

for ($i = 0; $i < 5; ++$i) {
    $harimau->serang($elang);
    echo "<br>";
    $elang->getInfoHewan();
    echo "<br>";
}
