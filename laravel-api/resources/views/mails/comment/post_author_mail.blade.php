<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail</title>
</head>
<body>
    Saudara/i <strong>{{ $comment->post->user->name }}</strong> artikel Anda yang berjudul "<strong>{{ $comment->post->title }}</strong>" telah dikomentari oleh <strong>{{ $comment->user->name }}</strong>.
</body>
</html>