export const CategoryComponent = {
    template: `
    <div class="row mt-4 justify-content-center">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-body">
                    <ul class="list-group">
                        <li v-for="(value, num) of post" class="list-group-item">
                            <strong>{{ num }}</strong> <br> {{ value }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    `,
    props: ['posts'],
    computed: {
        post() {
            return this.posts.filter((post) => {
                return post.id === parseInt(this.$route.params.id)
            })[0]
        }
    }
}