<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail</title>
</head>
<body>
    Saudara/i <strong>{{ $comment->user->name }}</strong>, Anda telah memberikan komentar pada artikel yang berjudul "<strong>{{ $comment->post->title }}</strong>" - <em>{{ $comment->post->user->name }}</em> dengan isi komentar : <em>"{{ $comment->content }}"</em>.
</body>
</html>