export const CategoriesComponent = {
    template: `
    <div class="row mt-4 justify-content-center">
        <div class="col-sm-10">
            <ul class="list-group">
                <li v-for="post of posts" class="list-group-item">
                    <router-link :to="'/category/' + post.id">{{ post.category }}</router-link>
                </li>
            </ul>
        </div>
    </div>
    `,
    props: ['posts']
}