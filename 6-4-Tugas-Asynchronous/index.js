// Soal 1
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Jawaban Soal 1
readBooks(10000, books[0], (timeRemaining0) => {
    readBooks(timeRemaining0, books[1], (timeRemaining1) => {
        readBooks(timeRemaining1, books[2], (timeRemaining2) => {
            readBooks(timeRemaining2, books[3], (timeRemaining3) => {
                console.log('')
            })
        })
    })
})

// let read = (time, books, i) => {
//     if (i < books.length) {
//         readBooks(time, books[i], (remaining) => {
//             if (remaining > 0) {
//                 i++;
//                 read(remaining, books, i);
//             }
//         })
//     }
// }

// read(10000, books, 0)