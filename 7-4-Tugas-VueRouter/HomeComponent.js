export const HomeComponent = {
    template: `
    <div class="row">
        <div class="col-sm-4 mt-4" v-for="(post, index) in posts" :key="index">
            <div class="card">
                <img src="https://dummyimage.com/16:9x1080" class="card-img-top" :alt="post.title">
                <div class="card-body">
                    <h4 class="card-title">{{ post.title }}</h4>
                    <p class="badge badge-success">{{ post.category }}</p>
                    <p class="card-text">{{ post.content }}</p>
                </div>
            </div>
        </div>
    </div>
    `,
    props: ['posts']
}