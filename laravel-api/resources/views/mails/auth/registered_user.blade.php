<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail</title>
</head>
<body>
    Selamat datang di {{ config('app.name') }}. Ini adalah kode OTP Anda : <strong>{{ $otp_code->otp }}</strong>. Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.
</body>
</html>